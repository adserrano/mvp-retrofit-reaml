package com.andres.currencyconvert.base;

import android.content.Context;

public interface IBaseView {
    Context getViewContext();

}
