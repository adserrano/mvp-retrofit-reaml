package com.andres.currencyconvert.base;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;


public class BaseActivity extends AppCompatActivity implements IBaseView {


    @Override
    public Context getViewContext() {
        return this;
    }



}
