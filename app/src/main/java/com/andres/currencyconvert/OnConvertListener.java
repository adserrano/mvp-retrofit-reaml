package com.andres.currencyconvert;

import java.util.Map;

/**
 * Created by andresdavid on 20/09/16.
 */
public interface OnConvertListener {

     void setErrorDolarAmount();
     void showResult();
}
