package com.andres.currencyconvert.storage.model;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by andresdavid on 21/09/16.
 */
public class Currency extends RealmObject {

    private  String base;
    private  String date;
    private RealmList<Rate> rate;

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public RealmList<Rate> getRate() {
        return rate;
    }

    public void setRate(RealmList<Rate> rate) {
        this.rate = rate;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
