package com.andres.currencyconvert;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;


import com.andres.currencyconvert.Currency.CurrencyPresenter;
import com.andres.currencyconvert.Currency.CurrencyPresenterImpl;
import com.andres.currencyconvert.Currency.CurrencyView;
import com.andres.currencyconvert.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity implements CurrencyView {

    private final String TAG_LOG = MainActivity.class.getName();

    private CurrencyPresenter presenter;
    @BindView(R.id.dolar_amout)
    EditText dolar_amout;
    @BindView(R.id.result_convertion)
    TextView result_convertion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        presenter = new CurrencyPresenterImpl(this);
        presenter.loadDataFromServer();
    }
    @OnClick(R.id.convert_button)
    public  void convert()
    {
        presenter.validate(dolar_amout.getText().toString());

    }

    @Override
    public void setErrorDolarAmount() {
        dolar_amout.setError("Campo Vacio!");
    }

    @Override
    public void showResult() {
        result_convertion.setText("Respuesta");
    }


    @Override
    public void onGetCurrencySuccess() {
        result_convertion.setText("onGetCurrencySuccess");

    }

    @Override
    public void onGetCurrencyFaild() {
        result_convertion.setText("onGetCurrencyFaild");
    }


}
