package com.andres.currencyconvert.Currency;

import com.andres.currencyconvert.Currency.Repository.CurrencyRepositoryImpl;
import com.andres.currencyconvert.OnConvertListener;

/**
 * Created by andresdavid on 20/09/16.
 */
public class CurrencyPresenterImpl implements CurrencyPresenter, OnConvertListener {

    private CurrencyView view;
    private CurencyInteractor curencyInteractor;

    public CurrencyPresenterImpl(CurrencyView view) {
        this.view = view;
        this.curencyInteractor = new CurencyInteractorImpl();
    }

    @Override
    public void validate(String dolars_amount) {
        if (view!=null) {
            curencyInteractor.validarCampos(dolars_amount, this);
        }

    }

    @Override
    public void loadDataFromServer() {
        if (view!=null) {
            CurrencyRepositoryImpl.getInstance(view.getViewContext()).getDataFromServer(this);
        }
    }

    @Override
    public void onUsersSuccess() {
        if (view != null)
            view.onGetCurrencySuccess();
    }

    @Override
    public void onUsersFail() {
        if (view != null)
            view.onGetCurrencyFaild();
    }

    @Override
    public void setErrorDolarAmount() {
        if (view != null)
            view.setErrorDolarAmount();
    }

    @Override
    public void showResult() {
        if (view != null)
            view.showResult();
    }

}
