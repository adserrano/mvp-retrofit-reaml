package com.andres.currencyconvert.Currency.Repository;

import android.content.Context;

import com.andres.currencyconvert.Currency.CurrencyPresenter;
import com.andres.currencyconvert.storage.database.RealmManager;
import com.andres.currencyconvert.rest.Currency.CurrencyResponse;
import com.andres.currencyconvert.rest.Currency.CurrencyService;
import com.andres.currencyconvert.rest.ServiceCreator;
import com.andres.currencyconvert.storage.model.Currency;
import com.andres.currencyconvert.storage.model.Rate;


import java.util.Map;

import io.realm.Realm;
import io.realm.RealmList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by andresdavid on 20/09/16.
 */
public class CurrencyRepositoryImpl implements CurrencyRepository {

    private final String BASE_CURRENCY ="USD";
    private Context mContext;
    private static CurrencyRepositoryImpl mCurrencyRepository;

    public static CurrencyRepositoryImpl getInstance(Context context) {
        if (mCurrencyRepository == null) {
            mCurrencyRepository = new CurrencyRepositoryImpl(context.getApplicationContext());
        }
        return mCurrencyRepository;
    }

    private CurrencyRepositoryImpl(Context context) {
        this.mContext = context;
    }

    @Override
    public void getDataFromServer(final CurrencyPresenter presenter) {
        CurrencyService userService = ServiceCreator.createService(CurrencyService.class);
        Call<CurrencyResponse> call = userService.getCurrencyDolars(BASE_CURRENCY);
        call.enqueue(new Callback<CurrencyResponse>() {
            @Override
            public void onResponse(Call<CurrencyResponse> call, Response<CurrencyResponse> response) {
                if (response.isSuccessful()) {
                    Currency currency = new Currency();
                    currency.setBase(response.body().getBase());
                    currency.setDate((response.body().getDate()));
                    RealmList<Rate> rateRealmList = new RealmList<>();
                    Map<String,Double> map =response.body().getRates();
                    for (Map.Entry<String,Double> entry : map.entrySet()) {
                        Rate rate = new Rate();
                        rate.setKey(entry.getKey());
                        rate.setValue(entry.getValue());
                        rateRealmList.add(rate);
                    }
                    currency.setRate(rateRealmList);
                    saveData(currency);
                    presenter.onUsersSuccess();
                } else {
                    presenter.onUsersFail();
                }
            }

            @Override
            public void onFailure(Call<CurrencyResponse> call, Throwable t) {
                presenter.onUsersFail();
            }
        });
    }

    @Override
    public void saveData(final Currency currencyData) {
        RealmManager.getInstance(mContext).getRealm().executeTransactionAsync(
                new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.insertOrUpdate(currencyData);
                    }
                }
        );
    }

    @Override
    public Currency getData() {
        return RealmManager.getInstance(mContext).getRealm().where(Currency.class).findFirst();
    }
}
