package com.andres.currencyconvert.Currency;

import android.text.TextUtils;

import com.andres.currencyconvert.OnConvertListener;

/**
 * Created by andresdavid on 20/09/16.
 */
public class CurencyInteractorImpl implements CurencyInteractor{


    @Override
    public void validarCampos(String amount, OnConvertListener listener) {

        if(TextUtils.isEmpty(amount))
        {
            listener.setErrorDolarAmount();
        }
        else
        {
            listener.showResult();
        }

    }
}
