package com.andres.currencyconvert.Currency;



/**
 * Created by andresdavid on 20/09/16.
 */
public interface CurrencyPresenter {

    void validate(String dolars_amount);
    void loadDataFromServer();
    void onUsersSuccess();
    void onUsersFail();

}
