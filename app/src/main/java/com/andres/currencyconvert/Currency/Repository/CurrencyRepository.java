package com.andres.currencyconvert.Currency.Repository;


import com.andres.currencyconvert.Currency.CurrencyPresenter;
import com.andres.currencyconvert.storage.model.Currency;

/**
 * Created by andresdavid on 20/09/16.
 */
public interface CurrencyRepository {

    void getDataFromServer(CurrencyPresenter presenter);
    void saveData(Currency currencyData);
    Currency getData();
}
