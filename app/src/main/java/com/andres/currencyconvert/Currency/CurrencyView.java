package com.andres.currencyconvert.Currency;

import com.andres.currencyconvert.base.IBaseView;

/**
 * Created by andresdavid on 20/09/16.
 */
public interface CurrencyView extends IBaseView {

    void setErrorDolarAmount();
    void showResult();
    void onGetCurrencySuccess();
    void onGetCurrencyFaild();

}
