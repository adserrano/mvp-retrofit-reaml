package com.andres.currencyconvert.Currency;


import com.andres.currencyconvert.OnConvertListener;

/**
 * Created by andresdavid on 20/09/16.
 */
public interface CurencyInteractor {

    void validarCampos(String  amount, OnConvertListener listener);

}
